package com.pgichure.springsamples.mysql.enums;

import lombok.Getter;

@Getter
public enum CustomerStatus {

    ACTIVE,INACTIVE, BLACKLISTED;

}