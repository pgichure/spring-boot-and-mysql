package com.pgichure.springsamples.mysql.dtos;

import java.util.UUID;

import com.pgichure.springsamples.mysql.enums.CustomerStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDto{

    private UUID id;

    private String firstName;
  
    private String lastName;

    private CustomerStatus status;
    
    private String postalAddress;
    
    private String postalCode;
  
    private String town;

    private String country;
  
}