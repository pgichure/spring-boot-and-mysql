package com.pgichure.springsamples.mysql.services;

import com.pgichure.springsamples.mysql.dtos.CustomerDto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.mysql.models.Customer;
import com.pgichure.springsamples.mysql.repositories.CustomerRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class CustomeService implements CustomerServiceI{

    private final CustomerRepository repository;

    @Override
    public CustomerDto save(CustomerDto dto){
        Customer customer = this.getCustomer(dto);
        customer = this.repository.save(customer);

        return this.getDto(customer);
    }

    @Override
    public List<CustomerDto> findAll(){
        List<Customer> customers = repository.findAll();

        return customers.stream()
                .map(customer -> this.getDto(customer))
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDto update(Long id, CustomerDto dto) throws Exception{
        Customer customer = repository.findById(id).orElseThrow(() -> new Exception("Customer not found for ID " + id));
        customer = this.getCustomer(dto);
        customer = repository.save(customer);
        return this.getDto(customer);
    }

    @Override
    public CustomerDto findById(Long id) throws Exception{
        Customer customer = repository.findById(id).orElseThrow(() -> new Exception("Customer not found for ID " + id));
        return this.getDto(customer);
    }

    @Override
    public CustomerDto delete(Long id) throws Exception{
        Customer customer = repository.findById(id).orElseThrow(() -> new Exception("Customer not found for ID " + id));
        CustomerDto dto = this.getDto(customer);
        repository.deleteById(id);
        return dto;
    }

    private CustomerDto getDto(Customer customer){

        return CustomerDto.builder()
                          .firstName(customer.getFirstName())
                          .id(customer.getId())
                          .lastName(customer.getLastName())
                          .postalAddress(customer.getPostalAddress())
                          .postalCode(customer.getPostalCode())
                          .town(customer.getTown())
                          .country(customer.getCountry())
                          .status(customer.getStatus())
                          .build();
    }

    private Customer getCustomer(CustomerDto dto){


        return Customer.builder()
                          .firstName(dto.getFirstName())
                          .id(dto.getId())
                          .lastName(dto.getLastName())
                          .postalAddress(dto.getPostalAddress())
                          .postalCode(dto.getPostalCode())
                          .town(dto.getTown())
                          .country(dto.getCountry())
                          .status(dto.getStatus())
                          .build();
    }

 
}