package com.pgichure.springsamples.mysql.services;

import java.util.List;

import com.pgichure.springsamples.mysql.dtos.CustomerDto;

public interface CustomerServiceI{
 
    public CustomerDto save(CustomerDto customer);

    public List<CustomerDto> findAll();

    public CustomerDto update(Long id, CustomerDto customer) throws Exception;

    public CustomerDto findById(Long id) throws Exception;

    public CustomerDto delete(Long id) throws Exception;
    
}