package com.pgichure.springsamples.mysql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pgichure.springsamples.mysql.models.Customer;
 
public interface CustomerRepository extends JpaRepository<Customer, Long> {
 
}