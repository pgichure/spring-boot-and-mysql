package com.pgichure.springsamples.mysql.models;


import java.util.UUID;

import com.pgichure.springsamples.mysql.enums.CustomerStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@Entity
@Table(name="customers") 
@AllArgsConstructor
@NoArgsConstructor
public class Customer{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="first_name")
    private String firstName;
  
    @Column(name="last_name")
    private String lastName;

    @Column(name="customer_status")
    @Enumerated(EnumType.STRING)
    private CustomerStatus status;
    
    @Column(name="postal_address")
    private String postalAddress;
  
    @Column(name="postal_code")
    private String postalCode;
  
    @Column(name="town")
    private String town;

    @Column(name="country")
    private String country;

    
  
}
