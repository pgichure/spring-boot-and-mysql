# Spring Boot And MySQL Integration
This project is an example of integrating Springboot & MySQL integration.

## Running the Project

```
cd working_directory
git clone https://gitlab.com/pgichure/spring-boot-and-mysql.git
cd spring-boot-and-mysql
mvn clean spring-boot-and-mysql
```
To see the report, go to target/site/jacoco/index.html and open the file to view the coverage